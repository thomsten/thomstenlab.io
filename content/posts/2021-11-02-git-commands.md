---
author: "Thomas"
title: "Git commands"
description: "Some useful aliases accumulated over time"
date: "2021-11-02"
tags: ["git", "code", "terminal"]
favorite: true
---

Over time, you realize there's a few `git` commands you're repeating over and
over again. One of these is

```sh
$ git push --set-upstream-to <upstream> <some new branch>
```

I like to be able to do small, self-contained, and simple pull/merge requests,
but cost the cost of changing branches, doing the work, committing, pushing,
merging -- and while doing this I keep forgetting the name of the of my branch,
or it's just long to write, I get it wrong, etc. That's why it's easier to do:

```sh
$ git push --set-upstream-to origin $(git branch --show-current)
```

or conveniently define an `alias`

```sh
$ alias git-psu="git push --set-upstream-to origin $(git branch --show-current)"
```
